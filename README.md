## About The Project

This repository contains the Back End part of the MERN stack project.
It expose an API to the clients and communicate with the database.

The Front End of this project is available [here.](https://gitlab.com/Depfrost/mernclient)

### Built With

* [NodeJs](https://nodejs.org/)
* [Express](https://expressjs.com/)
* [MongoDB](https://www.mongodb.com/)

## Getting Started

### Prerequisites

* Setup your MongoDB database and get your connection URL.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/Depfrost/mernserver
   ```
2. Rename the file `.env.exemple` to `.env` and provide the database URL, a port and a JWT secret.

4. Install Yarn packages
   ```sh
   yarn install
   ```
5. Run the project
   ```sh
	yarn start
	```


## License

Distributed under the MIT License.

## Acknowledgments

This project has been realized with the help of  the JavaScriptMastery's tutorial on MERN stack.